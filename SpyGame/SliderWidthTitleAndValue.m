//
//  SliderWidthTitleAndValue.m
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import "SliderWidthTitleAndValue.h"

@implementation SliderWidthTitleAndValue

- (instancetype)init: (NSString *) titleText prefixText:(NSString *) prefixText maxValue:(float) maxValue minValue:(float) minValue startValue:(float) startValue
{
    self = [super init];
    if (self){
        _title = [UILabel new];
        _title.text = titleText;
        _title.textColor = [[UIColor alloc] initWithDisplayP3Red:222/255.0 green:190 / 255.0 blue:124 / 255.0 alpha: 1.0];
        
        
        _slider = [UISlider new];
        _slider.maximumValue = maxValue;
        _slider.minimumValue = minValue;
        _slider.value = startValue;
        _slider.minimumTrackTintColor = [[UIColor alloc] initWithDisplayP3Red:198/255.0 green:158 / 255.0 blue:56 / 255.0 alpha: 1.0];
        _slider.maximumTrackTintColor = UIColor.whiteColor;
        
        [_slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        

        _count = [UILabel new];
        _count.text = @(startValue).stringValue;
        _count.textColor = [[UIColor alloc] initWithDisplayP3Red:222/255.0 green:190 / 255.0 blue:124 / 255.0 alpha: 1.0];
        
        _prefix = [UILabel new];
        _prefix.text = prefixText;
        _prefix.textColor = [[UIColor alloc] initWithDisplayP3Red:222/255.0 green:190 / 255.0 blue:124 / 255.0 alpha: 1.0];
        
        [self addSubview: _title];
        [self addSubview: _slider];
        [self addSubview: _count];
        [self addSubview: _prefix];

        _slider.translatesAutoresizingMaskIntoConstraints = NO;
        _title.translatesAutoresizingMaskIntoConstraints = NO;
        _count.translatesAutoresizingMaskIntoConstraints = NO;
        _prefix.translatesAutoresizingMaskIntoConstraints = NO;
        
    }
    return self;
}

- (void) setConstraints
{
    [self.title.topAnchor constraintEqualToAnchor: self.topAnchor constant: 16.0].active = YES;
    [self.title.centerXAnchor constraintEqualToAnchor: self.centerXAnchor].active = YES;
    [self.title.bottomAnchor constraintEqualToAnchor: self.slider.topAnchor constant: -16.0].active = YES;
    
    [self.slider.centerXAnchor constraintEqualToAnchor: self.centerXAnchor].active = YES;
    [self.slider.bottomAnchor constraintEqualToAnchor: self.count.topAnchor constant: -16.0].active = YES;
    [self.slider.leadingAnchor constraintEqualToAnchor: self.leadingAnchor constant: 40.0].active = YES;
    [self.slider.trailingAnchor constraintEqualToAnchor: self.trailingAnchor constant: -40.0].active = YES;
    [self.slider.widthAnchor constraintEqualToConstant:200.0].active = YES;
    
    [self.count.centerXAnchor constraintEqualToAnchor: self.centerXAnchor].active = YES;
    [self.count.bottomAnchor constraintEqualToAnchor: self.bottomAnchor constant: -16.0].active = YES;
    
    [self.prefix.bottomAnchor constraintEqualToAnchor: self.count.bottomAnchor].active = YES;
    [self.prefix.leadingAnchor constraintEqualToAnchor: self.count.trailingAnchor constant: 16].active = YES;
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    self.count.text = @(roundf(sender.value)).stringValue;
}

@end

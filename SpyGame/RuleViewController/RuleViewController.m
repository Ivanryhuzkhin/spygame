//
//  RuleViewController.m
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import "RuleViewController.h"

@interface RuleViewController ()

@end

@implementation RuleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.view.backgroundColor = UIColor.blackColor;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:  animated];
    [self.navigationController setNavigationBarHidden: NO];
    UITextView *rulesText = [UITextView new];
    
    rulesText.textColor = UIColor.whiteColor;
    rulesText.font = [UIFont systemFontOfSize: 20.0];
    rulesText.backgroundColor = UIColor.clearColor;
    rulesText.textAlignment = NSTextAlignmentJustified;
    rulesText.text = @"""Игроки просматривают свои роли и разделяются на два «лагеря»: цель шпиона - затеряться среди добропорядочных граждан неизвестной ему локации, а все остальные должны вычислить, кто же среди них «лишний». \r Поиск шпиона ведётся посредством оживлённой беседы между игроками. Все, кроме шпиона, знают, где находятся (например, в больнице), лишь шпион остаётся в неведении и должен догадаться по ходу разговора, куда его занесла «нелёгкая». В противном случае его вычислят. \r Поиск ведётся около пяти минут, далее все голосуют и стараются найти шпиона. В случае верного единогласного решения шпион отправляется в заточение и начинает следующий раунд в роли ведущего. Если игроки так и не пришли к единодушному решению, то шпион побеждает """;
    
    [self.view addSubview: rulesText];
    rulesText.translatesAutoresizingMaskIntoConstraints = NO;
    
    [rulesText.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor constant: 10].active = YES;
    [rulesText.leadingAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.leadingAnchor constant:16].active = YES;
    [rulesText.trailingAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.trailingAnchor constant: -16].active = YES;
    [rulesText.bottomAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.bottomAnchor constant: -10].active = YES;
}

@end

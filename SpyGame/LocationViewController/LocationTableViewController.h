//
//  LocationTableViewController.h
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationTableViewController : UITableViewController
+ (NSDictionary *) rolesDict;
@end

NS_ASSUME_NONNULL_END

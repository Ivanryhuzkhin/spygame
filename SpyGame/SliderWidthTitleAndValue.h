//
//  SliderWidthTitleAndValue.h
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SliderWidthTitleAndValue : UIView
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *prefix;
@property (nonatomic, strong) UILabel *count;
- (void) setConstraints;

- (instancetype)init: (NSString *) titleText prefixText:(NSString *) prefixText maxValue:(float) maxValue minValue:(float) minValue startValue:(float) startValue;

@end

NS_ASSUME_NONNULL_END

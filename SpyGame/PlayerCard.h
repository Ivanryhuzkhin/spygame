//
//  PlayerCard.h
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayerCard : UIView
    @property Boolean isFront;
    @property NSString *locationName;
    @property NSString *roleName;
    @property UIImage *frontImage;
    @property (nonatomic, strong) UIButton *backgroundButton;

    - (instancetype)init: (UIImage *) frontImage;

@end

NS_ASSUME_NONNULL_END

//
//  GameViewController.h
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GameViewController : UIViewController
-(instancetype)initGame:(NSInteger) civiliansNumber spysNumber:(NSInteger) spysNumber gameDuration:(NSInteger) gameDuration locationName:(NSString*) locationName frontImage:(UIImage *) frontImage;
@end

NS_ASSUME_NONNULL_END

//
//  GameViewController.m
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import "GameViewController.h"
#import "../PlayerCard.h"
#include <stdlib.h>

@interface GameViewController ()
@property  NSInteger civiliansNumber;
@property  NSInteger spysNumber;
@property  NSInteger gameDuration;
@property  NSArray <NSString *> *rolesNames;
@property  NSInteger spinsCount;
@property  NSString* locationName;
@property  NSInteger totalCardsNumber;
@property  NSTimer* gameDurationTimer;
@property  NSMutableArray <NSNumber *> *rolesIndexes;
@property (nonatomic, strong) UILabel *timerLabel;
@property (nonatomic, strong) UIImage *downloadedImage;
@end

@implementation GameViewController



-(instancetype)initGame:(NSInteger) civiliansNumber spysNumber:(NSInteger) spysNumber gameDuration:(NSInteger) gameDuration locationName:(NSString*) locationName frontImage:(UIImage *) frontImage
{
    self = [super init];
    if (self)
    {
        
        NSDictionary *rolesDict = @{
                                    @"Пиратский корабль" : @[@"Роль 1", @"Роль 2", @"Роль 3", @"Роль 4", @"Роль 5", @"Роль 6", @"Роль 7", @"Роль 8", @"Роль 9", @"Роль 10"],
                                    @"Самолет": @[@"Роль 1", @"Роль 2", @"Роль 3", @"Роль 4", @"Роль 5", @"Роль 6", @"Роль 7", @"Роль 8", @"Роль 9", @"Роль 10"],
                                    @"Космическая станция": @[@"Роль 1", @"Роль 2", @"Роль 3", @"Роль 4", @"Роль 5", @"Роль 6", @"Роль 7", @"Роль 8", @"Роль 9", @"Роль 10"],
                                    @"Банк": @[@"Роль 1", @"Роль 2", @"Роль 3", @"Роль 4", @"Роль 5", @"Роль 6", @"Роль 7", @"Роль 8", @"Роль 9", @"Роль 10"]
                                    };
        
        
        _civiliansNumber = civiliansNumber;
        _spysNumber = spysNumber;
        _gameDuration = gameDuration * 60;
        _locationName = locationName;
        _rolesNames = rolesDict[locationName];
        _totalCardsNumber = _spysNumber + _civiliansNumber;
        _spinsCount = 0;
        _downloadedImage = frontImage;
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    
    self.view.backgroundColor = UIColor.blackColor;
    
    self.rolesIndexes = [NSMutableArray new];
    NSInteger generatedSpysIndexes = 0;
    for ( int i = 0; i < self.totalCardsNumber; i++){
        if (arc4random_uniform(2) == 1 && generatedSpysIndexes < self.spysNumber)
        {
            [self.rolesIndexes addObject: @(-1)];
            generatedSpysIndexes += 1;
        }
        else{
            NSInteger tempGeneratedIndex = arc4random_uniform(self.civiliansNumber);
            Boolean isDouble = false;
            for (int j = 0; j < self.rolesIndexes.count; j++ )
            {
                if (tempGeneratedIndex == self.rolesIndexes[j].integerValue)
                {
                    isDouble = true;
                }
            }
            if (isDouble)
            {
                i -= 1 ;
                isDouble = false;
            }
            else
            {
                [self.rolesIndexes addObject: @(tempGeneratedIndex)];
            }
        }
    }
    // Если рандом не дал ни одного шпиона, то добавляем руками в конец массива.
    if (generatedSpysIndexes < self.spysNumber)
    {
        self.rolesIndexes[self.rolesIndexes.count - 1] = @(-1);
    }
    
    [self initPlayCard];
    
}

- (void) initPlayCard
{
    PlayerCard *mainCard = [[PlayerCard alloc] init: self.downloadedImage];
    UITapGestureRecognizer * cardTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onCardTap:)];
    [mainCard addGestureRecognizer: cardTapGesture];
    
    [self.view addSubview: mainCard];
    mainCard.translatesAutoresizingMaskIntoConstraints = NO;
    
    [mainCard.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor constant: 60].active = YES;
    [mainCard.leadingAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.leadingAnchor constant:40].active = YES;
    [mainCard.trailingAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.trailingAnchor constant: -40].active = YES;
    [mainCard.bottomAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.bottomAnchor constant: -60].active = YES;
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:  animated];
    [self.navigationController setNavigationBarHidden: NO];
}



- (void) onCardTap:(UITapGestureRecognizer *)recognizer
{
    PlayerCard *mainView = recognizer.view;
    
    [UIView transitionWithView:mainView  duration:0.6 options:(UIViewAnimationOptionTransitionFlipFromLeft) animations:^{
        if (mainView.isFront) {
            NSString *roleName;
            NSString *locationName =  [NSString stringWithFormat: @"Локация:\r%@", self.locationName];
            if (self.rolesIndexes[self.spinsCount].integerValue == -1)
            {
                locationName = @"Ваша роль:\rШпион";
                roleName = @"";
            }
            else
            {
                NSNumber *roleIndex = self.rolesIndexes[self.spinsCount];
                roleName = [NSString stringWithFormat: @"Ваша роль:\r%@", self.rolesNames[roleIndex.integerValue]];
                
            }
            
            mainView.roleName = roleName;
            mainView.locationName = locationName;
            self.spinsCount += 1;
        }
        mainView.isFront = !mainView.isFront;
        
    }  completion:^(BOOL finished) {
        if (self.spinsCount == self.totalCardsNumber && mainView.isFront) {
            [UIView animateWithDuration: 1 animations:^{
                mainView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            } completion:^(BOOL finished) {
                mainView.alpha = 0;
                
                [self prepareTimer];
            }];
        }
    }];
}

- (void) prepareTimer
{
    self.timerLabel = [UILabel new];
    self.timerLabel.text = [NSString stringWithFormat:@"%02ld:00",self.gameDuration / 60];
    self.timerLabel.font = [UIFont systemFontOfSize:50.0];
    self.timerLabel.textColor = [[UIColor alloc] initWithDisplayP3Red:198/255.0 green:158 / 255.0 blue:56 / 255.0 alpha: 1.0];
    [self.view addSubview: self.timerLabel];
    self.timerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.timerLabel.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor constant: 60].active = YES;
    [self.timerLabel.bottomAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.bottomAnchor constant: -60].active = YES;
    [self.timerLabel.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = YES;
    
    self.gameDurationTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f  target:self selector:@selector(updateGameDurationTimer) userInfo:nil repeats:YES];
}

- (void) updateGameDurationTimer
{
    if (self.gameDuration != 0)
    {
        self.gameDuration -= 1;
        NSInteger timerMinutes = self.gameDuration / 60;
        NSInteger timerSeconds = (self.gameDuration % 60);
        self.timerLabel.text = [NSString stringWithFormat: @"%02ld:%2ld", (long)timerMinutes, (long)timerSeconds];
    }
    else {
        [self.gameDurationTimer invalidate];
        self.timerLabel.text = @"Игра окончена";
    }
    
}

@end

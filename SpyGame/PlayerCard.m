//
//  PlayerCard.m
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import "PlayerCard.h"

@implementation PlayerCard

- (instancetype)init: (UIImage *) frontImage
{
    self = [super init];
    if (self) {
        _backgroundButton = [UIButton new];
        [_backgroundButton setBackgroundImage: frontImage forState:UIControlStateNormal];
        [_backgroundButton setUserInteractionEnabled:NO];
        _isFront = YES;
        _backgroundButton.titleLabel.numberOfLines = 0;
        _backgroundButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _backgroundButton.titleLabel.font = [UIFont systemFontOfSize:50.0];
        [self addSubview:_backgroundButton];
        
        _backgroundButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_backgroundButton setTitleColor: [[UIColor alloc] initWithDisplayP3Red:198/255.0 green:158 / 255.0 blue:56 / 255.0 alpha: 1.0] forState:UIControlStateNormal];
        
        [_backgroundButton.topAnchor constraintEqualToAnchor: self.topAnchor].active = YES;
        [_backgroundButton.bottomAnchor constraintEqualToAnchor: self.bottomAnchor].active = YES;
        [_backgroundButton.leadingAnchor constraintEqualToAnchor: self.leadingAnchor].active = YES;
        [_backgroundButton.trailingAnchor constraintEqualToAnchor: self.trailingAnchor].active = YES;
        
        _frontImage = frontImage;
        
        
    }
    return self;
}

 - (void)drawRect:(CGRect)rect
{
    if (self.isFront) {
        [self.backgroundButton setTitle: @"Tap" forState:UIControlStateNormal];
        [self.backgroundButton setBackgroundImage: self.frontImage forState:UIControlStateNormal];
        
    }
    else {
        [self.backgroundButton setBackgroundImage: [UIImage imageNamed:@"card"] forState:UIControlStateNormal];
        NSString *cardText = [NSString stringWithFormat: @"%@\r\r%@", self.locationName, self.roleName];
        [self.backgroundButton setTitle: cardText  forState:UIControlStateNormal];
    }
}

- (void)setIsFront:(Boolean*)isFront {
    _isFront = isFront;
    [self setNeedsLayout];
    [self setNeedsDisplay];
}

@end

//
//  ViewController.m
//  SpyGame
//
//  Created by Иван Рыжухин on 25/07/2019.
//  Copyright © 2019 Ryzhukhin. All rights reserved.
//

#import "ViewController.h"
#import "SliderWidthTitleAndValue.h"
#import "GameViewController/GameViewController.h"
#import "RuleViewController/RuleViewController.h"
#import "LocationViewController/LocationTableViewController.h"

#include <stdlib.h>

@interface ViewController ()
    
    @property (nonatomic, strong) UIButton *playButton;
    @property (nonatomic, strong) UIButton *ruleButton;
    @property (nonatomic, strong)  UIButton *locationsButton;
    @property SliderWidthTitleAndValue *playersView;
    @property SliderWidthTitleAndValue *timeView;
    @property UIImage *downloadedImage;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButtonItem;
    
    [self setupUIComponents];
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden: YES];
    [self someRandomStuff];
}


- (void) setupUIComponents
{
    self.playersView = [[SliderWidthTitleAndValue alloc] init:@"Количество игроков" prefixText: @"чел" maxValue: 10.0 minValue: 3.0 startValue: 6.0 ];
    [self.playersView setConstraints];
    
    
    self.timeView = [[SliderWidthTitleAndValue alloc] init:@"Продолжительность игры" prefixText: @"мин" maxValue: 15.0 minValue: 1.0 startValue: 5.0 ];
    [self.timeView setConstraints];
    
    [self.view addSubview: self.playersView];
    [self.view addSubview: self.timeView];
    
    self.playersView.translatesAutoresizingMaskIntoConstraints = NO;
    self.timeView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.playersView.topAnchor constraintEqualToAnchor: self.view.safeAreaLayoutGuide.topAnchor constant: 36.0].active = YES;
    [self.playersView.bottomAnchor constraintEqualToAnchor: self.timeView.topAnchor constant: -36.0].active = YES;
    [self.playersView.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = YES;
    
    [self.timeView.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = YES;
    
    [self prepareMenu];
}

- (void) prepareMenu
{
    self.playButton = [UIButton new];
    [self.view addSubview: self.playButton];
    self.playButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.playButton.backgroundColor = [[UIColor alloc] initWithDisplayP3Red:198/255.0 green:158 / 255.0 blue:56 / 255.0 alpha: 1.0];
    [self.playButton setTitle:@"Играть" forState: UIControlStateNormal];
    
    self.ruleButton = [UIButton new];
    [self.view addSubview: self.ruleButton];
    self.ruleButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.ruleButton.backgroundColor = [[UIColor alloc] initWithDisplayP3Red:198/255.0 green:158 / 255.0 blue:56 / 255.0 alpha: 1.0];
    [self.ruleButton setTitle:@"Правила" forState: UIControlStateNormal];
    
    
    self.locationsButton = [UIButton new];
    [self.view addSubview: self.locationsButton];
    self.locationsButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.locationsButton.backgroundColor = [[UIColor alloc] initWithDisplayP3Red:198/255.0 green:158 / 255.0 blue:56 / 255.0 alpha: 1.0];
    [self.locationsButton setTitle:@"Локации" forState: UIControlStateNormal];
    
    
    [self setMenuButtonConstraints:self.playButton];
    [self setMenuButtonConstraints:self.ruleButton];
    [self setMenuButtonConstraints:self.locationsButton];
    
    [self.ruleButton.bottomAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor constant: -40].active = YES;
    [self.ruleButton.topAnchor constraintEqualToAnchor:self.locationsButton.bottomAnchor constant: 40].active = YES;
    [self.locationsButton.topAnchor constraintEqualToAnchor:self.playButton.bottomAnchor constant: 40].active = YES;
    
    [self.playButton addTarget:self action:@selector(onPlayTap:) forControlEvents: UIControlEventTouchUpInside];
    [self.locationsButton addTarget:self action:@selector(onLocationTap:) forControlEvents: UIControlEventTouchUpInside];
    [self.ruleButton addTarget:self action:@selector(onRuleTap:) forControlEvents: UIControlEventTouchUpInside];
}



- (void) setMenuButtonConstraints: (UIButton *) button
{
    
    [button.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = YES;
    [button.heightAnchor constraintEqualToConstant: 60.0].active = YES;
    [button.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant: 40.0].active = YES;
    [button.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant: -40.0].active = YES;
    
}

- (IBAction) onPlayTap:(UIButton *) sender
{
    NSInteger playersNumber = [self.playersView.count.text integerValue];
    
    NSInteger spysNumber = ceil(playersNumber / 7.0);
    NSInteger civiliansNumber = playersNumber - spysNumber;
    NSInteger gameDuration = [self.timeView.count.text integerValue];
    
    NSArray <NSString *> *locationsList = @[@"Пиратский корабль", @"Самолет", @"Космическая станция", @"Банк"];
    NSUInteger locationIndex = arc4random_uniform(locationsList.count);
    NSString *locationName = locationsList[locationIndex];
    
    
    GameViewController *vc = [[GameViewController alloc] initGame:civiliansNumber spysNumber:spysNumber gameDuration:gameDuration locationName:locationName frontImage: self.downloadedImage];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction) onLocationTap:(UIButton *) sender
{
    LocationTableViewController *vc = [LocationTableViewController new];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction) onRuleTap:(UIButton *) sender
{
    RuleViewController *vc = [RuleViewController new];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) someRandomStuff
{
    UIImageView *randomImage = [UIImageView new];
    
    NSURL *apiString = [NSURL URLWithString: @"https://dog.ceo/api/breeds/image/random"];
    NSURLSession *urlSesion = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [urlSesion dataTaskWithURL:apiString completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //
        if (!error)
        {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            
            [[urlSesion downloadTaskWithURL: [NSURL URLWithString:json[@"message"]] completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                self.downloadedImage = [UIImage imageWithData:
                                        [NSData dataWithContentsOfURL:location]];
                
            }] resume];
        }
        else
        {
            NSLog(@"Error occured!");
        }
    }];
    
    
    [dataTask resume];
    
}

@end
